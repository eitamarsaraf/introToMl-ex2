import matplotlib.pyplot as plt
from util import *


def perceptron(x_data, y_data, cross_valid, x_test, y_test):
        max_acc = 0
        max_w = 0
        w = np.zeros((np.unique(y_data).size, (x_data.shape[1])))
        eta = 0.1
        epochs = 100
        train_acc= dict()
        valid_acc = dict()

        size = int(x_data.shape[0] / cross_valid)
        for e in range(epochs):
            eta = eta/10
            train_acc_cross = list()
            valid_acc_cross = list()

            for k in range(cross_valid):
                x_train = np.delete(x_data, np.s_[k * size:(k + 1) * size], axis=0)
                x_valid = x_data[k * size:(k + 1) * size]
                y_train = np.delete(y_data, np.s_[k * size:(k + 1) * size], axis=0)
                y_valid = y_data[k * size:(k + 1) * size]

                valid_acc_cross.append(0.0)

                for x, y in zip(x_valid, y_valid):
                    y_hat = np.argmax(np.dot(w, x))
                    if y == y_hat:
                        valid_acc_cross[k] += 1
                valid_acc_cross[k] = valid_acc_cross[k] / x_valid.shape[0]


                train_acc_cross.append(0.0)

                for x, y in zip(x_train, y_train):
                    y_hat = np.argmax(np.dot(w, x))
                    if y == y_hat:
                        train_acc_cross[k] += 1
                train_acc_cross[k] = train_acc_cross[k] / x_train.shape[0]


                """indexs = shuffle(x_train.shape[0])
                for i in indexs:
                    x = x_train[i]
                    y = y_train[i]"""
                for x, y in zip(x_train, y_train):
                    y_hat = np.argmax(np.dot(w, x))
                    if y != y_hat:
                        w[y,:] += eta*x
                        w[y_hat, :] -= eta*x

            train_acc[e] = np.mean(train_acc_cross)
            mean_valid_acc = np.mean(valid_acc_cross)
            valid_acc[e] = mean_valid_acc
            if mean_valid_acc > max_acc:
                max_acc = mean_valid_acc
                max_w = w


        plt.plot(list(valid_acc.keys()), list(valid_acc.values()), label= 'test')
        plt.plot(list(train_acc.keys()), list(train_acc.values()),  label='train')
        plt.legend()
        plt.show()
        accuracy = 0
        res = []
        for x, y in zip(x_test, y_test):
            y_hat = np.argmax(np.dot(max_w, x))
            res.append(y_hat)
            if y == y_hat:
                accuracy +=1
        print("test perceptron    ", accuracy/x_test.shape[0], end='\t')
        print('max acc = ', str(max_acc))
        return res


def pa(x_data, y_data, cross_valid, x_test, y_test):
    w = np.zeros((np.unique(y_data).size, (x_data.shape[1])))
    epochs = 150
    train_acc = dict()
    valid_acc = dict()
    max_acc = 0
    max_epoc = 0
    max_w = 0
    size = int(x_data.shape[0] / cross_valid)
    for e in range(epochs):

        train_acc_cross = list()
        valid_acc_cross = list()

        for k in range(cross_valid):
            x_train = np.delete(x_data, np.s_[k * size:(k + 1) * size], axis=0)
            x_valid = x_data[k * size:(k + 1) * size]
            y_train = np.delete(y_data, np.s_[k * size:(k + 1) * size], axis=0)
            y_valid = y_data[k * size:(k + 1) * size]

            valid_acc_cross.append(0.0)
            # x_train, y_train = sklearn.utils.shuffle(x_train, y_train, random_state=1)
            # for x,y in zip(x_train, y_train):

            for x, y in zip(x_valid, y_valid):
                y_hat = np.argmax(np.dot(w, x))
                if y == y_hat:
                    valid_acc_cross[k] += 1
            valid_acc_cross[k] = valid_acc_cross[k] / x_valid.shape[0]

            train_acc_cross.append(0.0)

            for x, y in zip(x_train, y_train):
                y_hat = np.argmax(np.dot(w, x))
                if y == y_hat:
                    train_acc_cross[k] += 1
            train_acc_cross[k] = train_acc_cross[k] / x_train.shape[0]

            """indexs = shuffle(x_train.shape[0])
            for i in indexs:
                x = x_train[i]
                y = y_train[i]"""
            for x, y in zip(x_train, y_train):
                # for x, y in zip(x_train, y_train):
                y_hat = np.argmax(np.dot(w, x))
                if y != y_hat:
                    l1 = np.dot(w[y], x)
                    l2 = np.dot(w[y_hat], x)
                    l = max(0, 1 - l1 + l2)
                    tau = (np.linalg.norm(x)) ** 2
                    tau = tau * 2
                    tau = l / tau
                    w[y, :] += tau * x
                    w[y_hat, :] -= tau * x

        train_acc[e] = np.mean(train_acc_cross)
        mean_valid_acc = np.mean(valid_acc_cross)
        valid_acc[e] = mean_valid_acc
        if mean_valid_acc > max_acc:
            max_acc = mean_valid_acc
            max_w = w
            max_epoc = e

    plt.plot(list(valid_acc.keys()), list(valid_acc.values()), label='test')
    plt.plot(list(train_acc.keys()), list(train_acc.values()), label='train')
    plt.legend()
    plt.show()
    accuracy = 0
    res = []
    for x, y in zip(x_test, y_test):
        y_hat = np.argmax(np.dot(max_w, x))
        res.append(y_hat)
        if y == y_hat:
            accuracy += 1
    print("test pa    " + str(accuracy / x_test.shape[0]), end='\t')
    print('max acc = ', str(max_acc), end='\t')
    print('max epoc = ', str(max_epoc))
    return res


def svm(x_train, y_train, x_test, y_test, eta, C, epochs):
    # x_train, y_train = shuffle_before_use(x_train, y_train)
    best_w = sgd_no_kernel(x_train, y_train, eta=eta, C=C, T=epochs)
    arr, test_avg = get_test_acc(best_w, x_test, y_test)
    print("test svm = " + str(test_avg))
    return arr


def get_test_acc(w, test_data, test_labels):
    return get_accuracy(w, test_data, test_labels)


def get_accuracy(w, validation_data, validation_labels):
    '''returns accuracy on the validation or test data, given the classifier weights'''
    err = 0
    arr = []
    for i in range(len(validation_data)):
        pred_ind = predict(w, validation_data[i])
        arr.append(pred_ind)
        if pred_ind != validation_labels[i]: # error in the prediction
            err = err+1
    return arr, 1-1.0*err/len(validation_data)


def predict(w, sample):
    ''' returns the prediction label'''
    w_best_ind = -1
    best = -999999
    for j in range(len(w)):
        pred = np.dot(w[j], sample)
        if pred >= best:
            w_best_ind = j
            best = pred
    return w_best_ind


def sgd_no_kernel(train_data, train_labels, eta=0.1, C=1, T=100000):
    '''implements SGD_no_kernel algorithm on training set and returns the W matrix'''
    w = np.zeros(shape=(len(np.unique(train_labels)), train_data.shape[1])) # initiate weights vectors to zeros
    w = w.astype(np.float)
    for t in range(T):
        # train_data, train_labels = shuffle_before_use(train_data, train_labels)
        for i in range(len(train_labels)):
            sample, sample_label = train_data[i, :], train_labels[i]
            j_max_ind = np.argmax(w.dot(sample))
            if j_max_ind != sample_label:
                w[j_max_ind] = (1 - C * eta) * w[j_max_ind] - eta * sample  # update using the derivative when j == yi
                w[sample_label] = (1 - C * eta) * w[sample_label] + eta * sample  # update using the derivative
                for w_index in range(len(w[:, 0])):
                    if w_index != sample_label and w_index != j_max_ind:
                        w[w_index] = (1 - C * eta) * w[w_index]
    return w


# def sapir_svm(x_data, y_data, cross_valid, x_test, y_test):
#     w = np.zeros((np.unique(y_data).size, (x_data.shape[1])))
#     lamda  = 25750#0.001
#     epochs = 1
#     train_acc= dict()
#     valid_acc = dict()
#     max_acc =0
#     max_w = 0
#     eta =1e-08 #1
#     size = int(x_data.shape[0] / cross_valid)
#     for e in range(epochs):
#         eta = eta/((e+1))
#         train_acc_cross = list()
#         valid_acc_cross = list()
#
#         for k in range(cross_valid):
#             x_train = np.delete(x_data, np.s_[k * size:(k + 1) * size], axis=0)
#             x_valid = x_data[k * size:(k + 1) * size]
#             y_train = np.delete(y_data, np.s_[k * size:(k + 1) * size], axis=0)
#             y_valid = y_data[k * size:(k + 1) * size]
#
#             valid_acc_cross.append(0.0)
#             # x_train, y_train = sklearn.utils.shuffle(x_train, y_train, random_state=1)
#             # for x,y in zip(x_train, y_train):
#
#             for x, y in zip(x_valid, y_valid):
#                 y_hat = np.argmax(np.dot(w, x))
#                 if y == y_hat:
#                     valid_acc_cross[k] += 1
#             valid_acc_cross[k] = valid_acc_cross[k] / x_valid.shape[0]
#
#             train_acc_cross.append(0.0)
#
#             for x, y in zip(x_train, y_train):
#                 y_hat = np.argmax(np.dot(w, x))
#                 if y == y_hat:
#                     train_acc_cross[k] += 1
#             train_acc_cross[k] = train_acc_cross[k] / x_train.shape[0]
#
#             indexs = shuffle(x_train.shape[0])
#             for i in indexs:
#                 x = x_train[i]
#                 y = y_train[i]
#             #for x, y in zip(x_train, y_train):
#                 # for x, y in zip(x_train, y_train):
#                 y_hat = np.argmax(np.dot(w, x))
#                 if y != y_hat:
#                     w[y, :] = (1 - eta * lamda) * w[y, :] + eta * x
#                     w[y_hat, :] = (1 - eta * lamda) * w[y_hat, :] - eta * x
#                     for i in range(w.shape[0]):
#                         if (i != y and i != y_hat):
#                             w[i, :] = (1 - eta * lamda) * w[i, :]
#
#         train_acc[e] = np.mean(train_acc_cross)
#         mean_valid_acc = np.mean(valid_acc_cross)
#         valid_acc[e] = mean_valid_acc
#         if mean_valid_acc > max_acc:
#             max_acc = mean_valid_acc
#             max_w = w
#
#     plt.plot(list(valid_acc.keys()), list(valid_acc.values()), label='test')
#     plt.plot(list(train_acc.keys()), list(train_acc.values()), label='train')
#     plt.legend()
#     plt.show()
#     accuracy = 0
#     res = []
#     for x, y in zip(x_test, y_test):
#         y_hat = np.argmax(np.dot(max_w, x))
#         res.append(y_hat)
#         if y == y_hat:
#             accuracy += 1
#     print("test svm    ", accuracy / x_test.shape[0])
#     return res


