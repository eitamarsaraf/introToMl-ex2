import sys
import algo
from util import *


if __name__ == "__main__":
    train_x_path = sys.argv[1]
    train_y_path = sys.argv[2]
    # test_x_path = sys.argv[3]

    train_x, train_y = read_input_file(train_x_path, train_y_path)
    train_x, train_y = shuffle_before_use(train_x, train_y)
    xTrain, yTrain, xTest, yTest = split_data(train_x, train_y)
    # xTrain, xTest = min_max(xTrain, xTest)

    res_perceptron = algo.perceptron(xTrain, yTrain, 10, xTest, yTest)

    xTrain_zScore, xTest_zScore = Z_score(xTrain, xTest)
    res_pa = algo.pa(xTrain_zScore, yTrain, 5, xTest_zScore, yTest)

    xTrain_std = xTrain
    xTest_std = xTest
    for i in range(len(train_x[0, :])):
        xTrain_std[:, i], xTest_std[:, i] = standardization(xTrain[:, i], xTest[:, i])
    res_svm = algo.svm(xTrain, yTrain, xTest, yTest, eta=1e-09, C=161617.0, epochs=4)

    for i in range(len(res_perceptron)):
        print('perceptron: %d, pa: %d, svm: %d, true: %d' %(res_perceptron[i],res_pa[i],res_svm[i], yTest[i]))
