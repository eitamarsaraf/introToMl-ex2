import numpy as np
import random


# methods on the data
def split_data(x, y):
    xTrain = x[:2600]
    xTest= x[2600:]
    yTrain = y[:2600]
    yTest = y[2600:]

    """xTrain = x[986:]
    xTest = x[:986]
    yTrain = y[986:]
    yTest = y[:986]"""
    return xTrain, yTrain, xTest, yTest


def shuffle(array_len):
    indexs = np.arange(array_len)
    for i in range(array_len-2):
        np.random.seed(1)
        j = random.randint(i+1, array_len-1)
        tmp = indexs[i]
        indexs[i] = indexs[j]
        indexs[j] = tmp
    return indexs


def shuffle_before_use(train_data, train_labels):
    indices = np.arange(train_labels.shape[0])
    np.random.shuffle(indices)
    train_data = train_data[indices]
    train_labels = train_labels[indices]
    return train_data, train_labels


def change_category(train_x):
    x_uniqe = np.unique(train_x[:, 0])
    i = 0
    for elem in x_uniqe:
        train_x[:, 0] = np.where(train_x[:, 0] == elem, i, train_x[:, 0])
        i += 0.5
    return train_x


# all normalization methods
def min_max(x, x_test):
    x = x.transpose()
    x_test = x_test.transpose()
    max_value = x.max(axis=1)
    min_value = x.min(axis=1)
    for i in range(x.shape[0]):
        if max_value[i] != min_value[i]:
            x[i]= (x[i] - min_value[i])/float(max_value[i]-min_value[i])
            x_test[i] = (x_test[i] - min_value[i]) / float(max_value[i] - min_value[i])
        elif max_value[i] > 1:
            x[i] = np.ones(x[i].shape)
            x_test[i] = np.ones(x[i].shape)
    return x.transpose(),  x_test.transpose()


def Z_score(x, x_test):
    x = x.transpose()
    x_test = x_test.transpose()

    for i in range(x.shape[0]):
        mean = np.mean(x[i])
        std = np.std(x[i])
        if std != 0:
            x[i] = (x[i]-mean)/float(std)
            x_test[i] = (x_test[i] - mean) / float(std)
        elif x[i][0] >1 or x[i][0]<0:
            x[i] = np.ones(x[i].shape)
            x_test[i] = np.ones(x_test[i].shape)

    return x.transpose(), x_test.transpose()


def standardization(feature_data_train, feature_data_test):
    mean = feature_data_train.mean()
    std = feature_data_test.std()
    feature_data_train = (feature_data_train - mean) / std
    feature_data_test = (feature_data_test - mean) / std
    return feature_data_train, feature_data_test


# read data
def read_input_file(train_x_path, train_y_path):
    with open(train_x_path, 'r') as train_x_file, open(train_y_path, 'r') as train_y_file:
        train_x = []
        train_y = []
        for line_x in train_x_file:
            train_x.append(line_x.strip().split(sep=','))
        for line_y in train_y_file:
            train_y.append(int(float(line_y.strip())))
        train_x = np.array(train_x)
        train_x = change_category(train_x)
        train_x = train_x.astype(np.float)
        train_y = np.asarray(train_y)
        return train_x, train_y