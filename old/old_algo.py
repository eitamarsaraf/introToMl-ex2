# import numpy as np
# from numpy import linalg as LA
#
# def perceptron(x_train, y_train, x_test, y_test):
#     w = np.zeros((3, x_train.shape[1]))
#     eta = 0.2
#     epochs = 500
#     for e in range(epochs):
#         x_train, y_train = sklearn.utils.shuffle(x_train, y_train, random_state=1)
#         for x,y in zip(x_train, y_train):
#             y_hat = np.argmax(np.dot(w, x))
#             if y != y_hat:
#                 w[y,:] += eta*x
#                 w[y_hat, :] -= eta*x
#
#         if e%10 == 0:
#             accuracy = 0
#             for x, y in zip(x_train, y_train):
#                 y_hat = np.argmax(np.dot(w, x))
#                 if y == y_hat:
#                     accuracy += 1
#             print(accuracy / y_train.shape[0])
#
#     accuracy = 0
#     res = []
#     for x, y in zip(x_test, y_test):
#         y_hat = np.argmax(np.dot(w, x))
#         res.append(y_hat)
#         if y == y_hat:
#             accuracy +=1
#     print(accuracy/y_test.shape[0])
#     return  res
#
#
# def pa(x_train, y_train, x_test, y_test):
#     w = np.zeros((3, x_train.shape[1]))
#     epochs = 500
#     for e in range(epochs):
#         x_train, y_train = sklearn.utils.shuffle(x_train, y_train, random_state=1)
#         for x,y in zip(x_train, y_train):
#             y_hat = np.argmax(np.dot(w, x))
#             if y != y_hat:
#                 l1 = np.dot(w[y],x)
#                 l2 = np.dot(w[y_hat],x)
#                 l = max(0, 1-l1+l2)
#                 tau = l/(LA.norm(x))**2
#                 w[y,:] += tau*x
#                 w[y_hat, :] -= tau*x
#
#         if e%10 == 0:
#             accuracy = 0
#             for x, y in zip(x_train, y_train):
#                 y_hat = np.argmax(np.dot(w, x))
#                 if y == y_hat:
#                     accuracy += 1
#             print(accuracy / y_train.shape[0])
#
#     accuracy = 0
#     res = []
#     for x, y in zip(x_test, y_test):
#         y_hat = np.argmax(np.dot(w, x))
#         res.append(y_hat)
#         if y == y_hat:
#             accuracy +=1
#     print(accuracy/y_test.shape[0])
#
#     return res
