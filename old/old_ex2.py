# import sys
# import old_algo
# from test import svm_find_params, svm
# from util import *
#
#
# def main():
#     pass
#
#
# if __name__ == "__main__":
#
#     # read input data
#     train_x_path = sys.argv[1]
#     train_y_path = sys.argv[2]
#     train_x, train_y = read_input_file(train_x_path, train_y_path)
#     # test_x_path = sys.argv[3]
#     # train_x, train y, test_x = read_input_file(train_x_path, train_y_path, test_x_path)
#
#     # normalize all data by MinMax method
#     for i in range(len(train_x[0, :])):
#         # train_x[:, i] = min_max_scaling(train_x[:, i])
#         train_x[:, i] = standardization(train_x[:, i])
#
#     # split the data to train, test and val
#     xTrain, yTrain, xTest, yTest = split_data(train_x, train_y)
#     # xTest, yTest, xVal, yVal = split_data_val(xTest, yTest)
#
#     # SVM
#     a = []
#     for i in range(60):
#         a.append(svm(xTrain, yTrain, xTest, yTest, eta=1e-09, C=161617.0, epochs=4))
#     print('mean = ', str(np.mean(a)))
#     print('std =  ', str(np.std(a)))
#     # test to find best params
#     # sumtmp = 0
#     # for stam in range(30):
#     #     train_x, train_y = shuffle_before_use(train_x, train_y)
#     #     xTrain, yTrain, xTest, yTest = split_data(train_x, train_y)
#     #     xTest, yTest, xVal, yVal = split_data_val(xTest, yTest)
#     #     tmp, best_eta, best_C, best_c_avg = svm_find_params(xTrain, yTrain, xTest, yTest, xVal, yVal, 4, 1e-09, stam)
#     # res_perceptron = algo.perceptron(xTrain, yTrain,  xTest, yTest)
#     # res_pa = algo.pa(xTrain, yTrain, xTest, yTest)
#     # for i in range(len(res_pa)):
#     #     print('perceptron: %d, pa: %d' %(res_perceptron[i], res_pa[i]) )
#
#     # main()