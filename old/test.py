from util import *

#
# def sgd_and_acc(train_data, train_labels, validation_data, validation_labels, eta=0.1, C=1, T=100000):
#     w = sgd_no_kernel(train_data, train_labels, eta=eta,C=C, T=T)
#     return get_accuracy(w,validation_data, validation_labels), get_accuracy(w,train_data, train_labels)


# def search_fot_params(train_data, train_labels, validation_data, validation_labels, eta, T=5):
#     eta_values = [1e-6, 1e-9]
#     C_values = [1, 10e5]
#     best_c_sum = 0
#     best_c = -1
#     best_eta = eta
#     val_err_eta, train_err_eta = [], []
#     val_err_C, train_err_C = [], []
#     for C in np.linspace(C_values[0], C_values[1], num=100):
#         C_sum = 0
#         for i in range(30):
#             acc_val, acc_train = sgd_and_acc(train_data, train_labels, validation_data, validation_labels, eta=eta, C=C,
#                                              T=T)
#             C_sum += acc_val
#             print('the result for c = ', str(C), '\tin iteration number: ', str(i), '\tis: ', str(acc_val))
#             # if acc_val > best_acc_val:
#             #     best_acc_val = acc_val
#             #     best_C = C
#             #     best_eta = eta
#         C_sum = C_sum/30
#         if C_sum > best_c_sum:
#             best_c_sum = C_sum
#             best_c = C
#         # print('for value c = ', str(C), '\tthe val acc avg was: ', str(C_sum))
#     # best_C = list(C_values)[val_err_C.index(min(val_err_C))]
#     # best_eta = list(eta_values)[val_err_eta.index(min(val_err_eta))]
#     # print('best acc_val: ', best_acc_val)
#     # print('best C: ', best_C)
#     # print('best eta: ', best_eta)
#     return best_eta, best_c, best_c_sum, C_values, val_err_eta, train_err_eta, val_err_C, train_err_C

# def svm_find_params(x_train, y_train, x_test, y_test, x_val, y_val, epochs, eta, num_of_run):
#     best_eta, best_C, best_c_avg, C_values, val_err_eta, train_err_eta, val_err_C,\
#     train_err_C = search_fot_params(x_train, y_train, x_val, y_val, eta, T=epochs)
#     output = 'train model with eta = ' + str(best_eta) + '  and c = ' + str(best_C) + ' becuase avg was: ' + str(best_c_avg)
#     best_w = sgd_no_kernel(x_train, y_train, eta=best_eta, C=best_C, T=epochs)
#     tmp = q6_c_acc_on_test(best_w, x_test, y_test)
#     output += ' the test num ' + str(num_of_run) + ' result was: ' + str(tmp)
#     # print('check iteration: ', str(i), '\tin space: ', str(space), 'for stats iter: ', str(stam))
#     # print("test acc = " + str(tmp))
#     # print('predict: ')
#     # print(np.array(arr))
#     # print('real: ')
#     # print(y_test)
#     return tmp, best_eta, best_C, best_c_avg
